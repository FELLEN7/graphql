const express = require(`express`);
const graphqlHTTP = require('express-graphql')
const schema = require(`../schema/schema`);
const mongoose = require(`mongoose`);

const app = express();
const PORT = 3000;

mongoose.connect('mongodb+srv://kalinichenko:dkGTDIH4UqWOVNIz@cluster0-mxkhv.mongodb.net/graph_ql?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true });

app.use(`/graphql`, graphqlHTTP({
  schema,
  graphiql: true
}));

const dbConnection = mongoose.connection;
dbConnection.on('error', err => console.log(`Connection error: ${err}`))
dbConnection.once('open', () => console.log(`Connected to DB!`))

app.listen(PORT, err => {
  console.log(err ? error : `Server started!`)
})